import requests
import curlify
import logging
import tempfile


class Client(object):
    def __init__(self, url, ca=None, auth=None, impersonate=None):
        """
        Elasticsearch Client
        """
        headers = {'Content-Type': 'application/json'}
        if impersonate:
            headers['opendistro_security_impersonate_as'] = impersonate

        with tempfile.NamedTemporaryFile(delete=False) as ca_f:
            ca_f.write(bytes(ca, 'utf-8'))
            self.s = requests.Session()
            self.url = url
            self.s.auth = (auth['username'], auth['password'])
            self.s.verify = ca_f.name
            self.s.headers = headers
            self.ca = ca

    def query(self, *args, **kwargs):
        """Query ElasticSearch

        """

        path = args[0]
        verb = kwargs.get('verb', 'GET')
        data = kwargs.get('data', {})

        r = self.s.request(verb,
                           '%s/%s' % (self.url, path),
                           params={},
                           json=data)
        logging.debug(curlify.to_curl(r.request))

        try:
            rj = r.json()
            if 'error' in rj:
                for root_cause in rj['error']['root_cause']:
                    logging.error("%s" % root_cause)
        except Exception as e:
            logging.trace("Exception: %s" % e)
            pass
        r.raise_for_status()

        return r

    def get_health(self):
        r = self.query("_cat/health")
        return r

    def get_roles(self):
        r = self.query("_opendistro/_security/api/roles/")
        return r.json()

    def get_role_mapping(self):
        r = self.query("_opendistro/_security/api/rolesmapping/")
        return r.json()

    def get_indices(self):
        r = self.query("_cat/indices?format=json")
        return r.json()

    def get_indices_names(self):
        r = []
        for item in self.get_indices():
            r.append(item['index'])
        r.sort()
        return r

    def delete_role_mapping(self, name):
        r = self.query("_opendistro/_security/api/rolesmapping/%s" % name,
                       verb='DELETE')
        return r

    def get_role_mapping_names(self):
        r = list(self.get_role_mapping().keys())
        r.sort()
        return r

    def get_role_names(self):
        r = list(self.get_roles().keys())
        r.sort()
        return r

    def create_role_mapping(self, role_name, backend_role):
        data = {"backend_roles": [backend_role], "hosts": [], "users": []}
        r = self.query("_opendistro/_security/api/rolesmapping/%s" % role_name,
                       data=data,
                       verb='PUT')
        return r

    def create_role(self, role_name, role_type):

        role_types = {
            'admin': {
                'cluster_permissions': ['*'],
                'description':
                'Custom role for %s: Allow full access to all indices and all cluster APIs'
                % role_name,
                'index_permissions': [{
                    'allowed_actions': ['*'],
                    'fls': [],
                    'index_patterns': ['*'],
                    'masked_fields': []
                }],
                'tenant_permissions': [{
                    'allowed_actions': ['kibana_all_write'],
                    'tenant_patterns': ['*']
                }]
            },
        }

        data = role_types.get(role_type)

        r = self.query('_opendistro/_security/api/roles/%s' % role_name,
                       verb='PUT',
                       data=data)
        return r
