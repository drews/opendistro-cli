#!/usr/bin/env python3
import sys
import argparse
import logging
import os
import hvac
import pprint
from opendistropy.es import Client


def parse_args():
    parser = argparse.ArgumentParser(description='Query elasticsearch')
    parser.add_argument('-v',
                        '--verbose',
                        help='Verbose output',
                        action='store_true')

    parser.add_argument('-i',
                        '--impersonate',
                        help='Impersonate another user',
                        type=str)
    subparsers = parser.add_subparsers(dest='action', help='sub-command help')
    subparsers.required = True

    # Roles stuff first
    list_roles = subparsers.add_parser('list-roles', help='List all roles')
    list_roles.add_argument('-d',
                            '--details',
                            help='Include details',
                            action='store_true')

    create_role = subparsers.add_parser('create-role',
                                        help='Create a new role')
    create_role.add_argument('-t',
                             '--role-type',
                             help="Type of role to create",
                             required=True,
                             choices=["admin"])
    create_role.add_argument('name', type=str, help="Name of the new role")

    # Role Mapping Stuff goes here
    list_role_mapping = subparsers.add_parser('list-role-mapping',
                                              help='List Role Mapping')
    list_role_mapping.add_argument('-d',
                                   '--details',
                                   help='Include details',
                                   action='store_true')

    delete_role_mapping = subparsers.add_parser('delete-role-mapping',
                                                help='Delete a role mapping')
    delete_role_mapping.add_argument('role_mapping',
                                     help='Target role mapping to delete',
                                     type=str)

    create_role_mapping = subparsers.add_parser(
        'create-role-mapping', help='Create a new role mapping')
    create_role_mapping.add_argument('role_mapping',
                                     help='Target role mapping to create',
                                     type=str)
    create_role_mapping.add_argument('backend_role',
                                     help='Backend role for new mapping')

    # Index stuff
    list_indices = subparsers.add_parser('list-indices',
                                         help='List All indices/indexes')
    list_indices.add_argument('-d',
                              '--details',
                              help='Include details',
                              action='store_true')

    return parser.parse_args()


def main():
    args = parse_args()

    if args.verbose:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)

    es_instance = os.environ.get('ES_INSTANCE', 'development')
    vault = hvac.Client(url=os.environ.get('VAULT_ADDR'))
    if 'VAULT_TOKEN' in os.environ:
        sys.stderr.write("Using VAULT_TOKEN auth\n")
        vault.token = os.environ['VAULT_TOKEN']
    else:
        sys.stderr.write("Don't know how to connect to vault, quitting\n")
        return 5

    es_auth = vault.read("secret/elastic/%s/admin" % es_instance)['data']
    es_ca = vault.read("secret/elastic/%s/root-ca" %
                       es_instance)['data']['cert']
    es = Client(es_auth['url'],
                ca=es_ca,
                impersonate=args.impersonate,
                auth={
                    'username': es_auth['username'],
                    'password': es_auth['password']
                })

    pp = pprint.PrettyPrinter(indent=2)

    if args.action == 'list-roles':
        if args.details:
            pp.pprint(es.get_roles())
        else:
            for item in (es.get_role_names()):
                print(item)
    elif args.action == 'create-role':
        if args.name in es.get_role_names():
            logging.error("%s already exists" % args.name)
            return 5

        es.create_role(args.name, args.role_type)
    elif args.action == 'list-role-mapping':
        if args.details:
            pp.pprint(es.get_role_mapping())
        else:
            for item in (es.get_role_mapping_names()):
                print(item)
    elif args.action == 'delete-role-mapping':
        if args.role_mapping not in es.get_role_mapping_names():
            logging.error("%s does not exist" % args.role_mapping)
            return 5
        es.delete_role_mapping(args.role_mapping)
    elif args.action == 'create-role-mapping':
        if args.role_mapping in es.get_role_mapping_names():
            logging.error("%s already exists" % args.name)
            return 5
        if args.backend_role not in es.get_role_names():
            logging.error("%s does not exist" % args.backend_role)
            return 5
        es.create_role_mapping(args.role_mapping, args.backend_role)
    elif args.action == 'list-indices':
        if args.details:
            pp.pprint(es.get_indices())
        else:
            for item in (es.get_indices_names()):
                print(item)

    return 0


if __name__ == "__main__":
    sys.exit(main())
